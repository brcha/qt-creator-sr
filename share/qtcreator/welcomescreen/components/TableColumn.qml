import QtQuick 1.0

QtObject {
    property string caption
    property string property
    property int width: 160
    property bool visible: true
    property int elideMode: Text.ElideRight
}
